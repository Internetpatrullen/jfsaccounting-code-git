# jfsaccounting-code-git

Fork of the accounting application Fribok. 

Original Subversion repo: https://sourceforge.net/projects/jfsaccounting/


Made a 'svn export' to get files from trunk, checked that it built, committed to Git. 

Not sure whether the original maintainers had a reason to stay on SVN or not. I just want to hack a bit on this project and I'm quite a bit more comfortable in Git. 


'mvn install' should setup and build, then you can run the JAR. Easy peasy. 


TODO:

https://butik.friprogramvarusyndikatet.se/blog/fribok/vagen-mot-fribok-3-0.html




Related projects:

https://github.com/skymandr/nordea2fribok
