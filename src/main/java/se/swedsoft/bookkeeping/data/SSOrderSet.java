package se.swedsoft.bookkeeping.data;

import java.util.List;

public class SSOrderSet {
    private final List<SSOrder> iOrders;

    public SSOrderSet(List<SSOrder> orderList) {
        iOrders = orderList;
    }

    public String commaJoinOrderNumbers() {
        StringBuilder sb = new StringBuilder();
        for (SSOrder iOrder : iOrders) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(iOrder.getNumber());
        }

        return sb.toString();
    }
}
