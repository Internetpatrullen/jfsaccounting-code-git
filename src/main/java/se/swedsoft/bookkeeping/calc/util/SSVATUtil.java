package se.swedsoft.bookkeeping.calc.util;


import se.swedsoft.bookkeeping.calc.math.SSAccountMath;
import se.swedsoft.bookkeeping.calc.math.SSVoucherMath;
import se.swedsoft.bookkeeping.data.SSAccount;
import se.swedsoft.bookkeeping.data.SSVoucher;
import se.swedsoft.bookkeeping.data.SSVoucherRow;
import se.swedsoft.bookkeeping.data.system.SSDB;
import se.swedsoft.bookkeeping.gui.util.SSBundle;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * $Id: SSVATUtil.java 229 2018-11-28 17:54:36Z ellefj $
 */
public class SSVATUtil {
    private SSVATUtil() {}

    /**
     * @param creditMinusDebetSum The map of the summirised vouchers for the account
     *
     * @return The result
     */
    public static BigDecimal getVatToPayOrRetrieve(Map<SSAccount, BigDecimal> creditMinusDebetSum) {
	// Lagt till momskoder för importmoms: UI1 UI2 UI3
        return SSAccountMath.getSumByVATCodeForAccounts(creditMinusDebetSum, "U1", "UVL",
                "U2", "U3", "UEU", "UTFU", "I", "IVL", "UI1", "UI2", "UI3");
    }

    /**
     * @param creditMinusDebetSum The map of the summirised vouchers for the account
     *
     * @return The rounded result
     */
    public static BigDecimal getVatToPayOrRetrieveRounded(Map<SSAccount, BigDecimal> creditMinusDebetSum) {
        return getVatToPayOrRetrieve(creditMinusDebetSum).setScale(0, RoundingMode.DOWN);
    }

    /**
     * @param name
     * @param iDateFrom
     * @param iDateTo
     * @param iAccountR1 The account with the VAT code R1
     * @param iAccountR2 The account with the VAT code R2
     * @param iAccountA The account with the VAT code A
     *
     * @return The voucher
     */
    public static SSVoucher generateVATVoucher(String name, Date iDateFrom, Date iDateTo, SSAccount iAccountR1, SSAccount iAccountR2, SSAccount iAccountA) {

        DateFormat iFormat = DateFormat.getDateInstance(DateFormat.SHORT);

        String iDescription = String.format(
                SSBundle.getBundle().getString("vatreport2015.voucherdescription"),
                iFormat.format(iDateFrom), iFormat.format(iDateTo));
	// Lagt till momskoder för importmoms: UI1 UI2 UI3
        List<SSAccount> iAccounts = SSAccountMath.getAccountsByVATCode(
                SSDB.getInstance().getAccounts(), "U1", "U2", "U3", "UVL", "UEU", "UTFU",
                "I", "IVL", "UI1", "UI2", "UI3");
        List<SSVoucher> iVouchers = SSVoucherMath.getVouchers(
                SSDB.getInstance().getVouchers(), iDateFrom, iDateTo);

        Map<SSAccount, BigDecimal> iCreditMinusDebetSum = SSVoucherMath.getCreditMinusDebetSum(
                iVouchers);

        SSVoucher    iVoucher = new SSVoucher();

        iVoucher.doAutoIncrecement();
        iVoucher.setDescription(iDescription);
        iVoucher.setDate(iDateTo);

        BigDecimal iSum = new BigDecimal(0);
        SSVoucherRow iRow;

        for (SSAccount iAccount : iAccounts) {

            BigDecimal iValue = iCreditMinusDebetSum.get(iAccount);

            if (iValue == null || iValue.signum() == 0) {
                continue;
            }

            iRow = new SSVoucherRow();
            iRow.setAccount(iAccount);
            iRow.setValue(iValue);

            iVoucher.addVoucherRow(iRow);

            iSum = iSum.add(iValue);
        }

        if (iSum.signum() != 0) {
            iRow = new SSVoucherRow();

            BigDecimal iRounded = iSum.setScale(0, RoundingMode.DOWN);

            if (iRounded.signum() > 0) {
                iRow.setAccount(iAccountR2);
                iRow.setCredit(iRounded);
            } else {
                iRow.setAccount(iAccountR1);
                iRow.setDebet(iRounded.abs());
            }

            iVoucher.addVoucherRow(iRow);

            if (iRounded.subtract(iSum).signum() != 0) {
                iRow = new SSVoucherRow();
                iRow.setAccount(iAccountA);
                iRow.setValue(iRounded.subtract(iSum));

                iVoucher.addVoucherRow(iRow);
            }
        }

        return iVoucher;
    }

}
